/*
 * Test setting pins high and low without hard-coding ports and pins, which is
 * needed to re-use SPI code without having to create copies of it, each 
 * intended for a specific PIC on-board Tenkou.
 * 
 * File:   test.c
 * Author: alek
 *
 * Created on 28 June 2017, 13:56
 */
//#define DEBUG // Compile in debug configuration?

// PIC16F877A Configuration Bit Settings
// 'C' source line config statements
#ifdef DEBUG
    #pragma config FOSC = HS     // Oscillator Selection bits (RC oscillator)
#else
    #pragma config FOSC = HS
#endif
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF //  // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOREN = OFF //   // Brown-out Reset Enable bit (BOR disabled)
#pragma config LVP = OFF     // Low-Voltage (Single-Supply) In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data EEPROM Memory Code Protection bit (Data EEPROM code protection off)
#pragma config WRT = OFF        // Flash Program Memory Write Enable bits (Write protection off; all program memory may be written to by EECON control)
#pragma config CP = OFF         // Flash Program Memory Code Protection bit (Code protection off)

#define _XTAL_FREQ 8000000

#include <xc.h>
#include "TenkouGPIO.h"
#include <pic16f877a.h>


int test_setpin(void){
    TRISB = 0; // Set used ports as outputs.
    TRISD = 0;
    PORTD = 0; // Initialise used ports.
    PORTB = 0;
    // Try max and min pin indices.
    for(int i = 0;i < 10;i++)//TODO what is the max. pin index?
    {
        setPinHigh(&PORTD,0); //RD0 = 1
        setPinHigh(&PORTD,7); //RD2 = 1
        #ifndef DEBUG // Delay doesn't work in debug, don't know why.
	        __delay_ms(500);
	    #endif
        setPinLow(&PORTD,0);  //RD0 = 0
        setPinLow(&PORTD,7);  //RD7 = 0
        #ifndef DEBUG
        	__delay_ms(500);
        #endif
    }
    // Try a different port.
    for(int i = 0;i < 10;i++)
    {
        setPinHigh(&PORTB,0);
        setPinHigh(&PORTB,7);
        #ifndef DEBUG
        	__delay_ms(500);
        #endif
        setPinLow(&PORTB,0);
        setPinLow(&PORTB,7);
        #ifndef DEBUG
	        __delay_ms(500);
	    #endif
    }
    return 0;
}

int test_readPin(void)
{
    TRISD0 = 0;
    TRISD1 = 0;
    TRISD2 = 0;
    PORTD = 0;
    RD0 = 1;
    for(int j=0; j<10;j++){
        if(readPin(&PORTD,0)){ //if RD0 is High then RD1 run.
            RD1 = 1;
            __delay_us(20);
            RD1 = 0;
            RD0 = 0;
        }
        else if(!readPin(&PORTD,0)){ //if RD0 is Low then RD2 run.
            RD2 = 1;
            __delay_us(20);
            RD2 = 0;
            RD0 = 1;
        }
    }
    return 0;
}

int test_readPinManualInput(void)
{
    TRISD0 = 0;
    TRISD1 = 0;
    TRISD2 = 0;
    PORTD = 0;
    for(int j=0; j<10; j++)
    {
        if(readPin(&PORTD,0))
        {
            RD1=1;
            __delay_us(50);
            RD1=0;
        }
        else
        {
            RD2=1;
            __delay_us(50);
            RD2=0;
        }
    }
    return 0;
}

int main(void)
{
    while(1) // Run all the tests continuously.
    {
        int a = test_setpin();
        int b = test_readPin();
        int d = test_readPinManualInput();
    }
}
