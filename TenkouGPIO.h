/* Functions that can set arbitrary pins to 0 or 1. Need this to make the digital
 * communication protocols (SPI, I2C etc.) code portable when working with varying
 * numbers of slaves per PIC and certain slaves being both masters and slaves
 * on different SPI buses.
 * 
 * Author: Alek, Urakami
 * Comments:
 * Revision history: 
 */
#ifndef TENKOU_GPIO_LIB_H
#define	TENKOU_GPIO_LIB_H

#include <xc.h>

void setPinHigh(volatile unsigned char *port, unsigned char pin);
void setPinLow(volatile unsigned char *port, unsigned char pin);
int readPin(volatile unsigned char *port, unsigned char pin);

#endif // End header guard.
