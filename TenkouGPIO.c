#include "TenkouGPIO.h"

/* Set a given pin from the chosen port to 1 (high). 
 * 
 * Attributes:
 * ------------
 *  * port: which IO port to use, e.g. A or B,
 *  * pin: index of the pin from the port, [0,7].
 */
void setPinHigh(volatile unsigned char *port, unsigned char pin)
{
    *port |= (unsigned char)(0x01 << pin);
}

/* Set a given pin from the chosen port to 0 (low). 
 * 
 * Attributes:
 * ------------
 *  * port: which IO port to use, e.g. A or B,
 *  * pin: index of the pin from the port, [0,7].
 */
void setPinLow(volatile unsigned char *port, unsigned char pin)
{
    *port &= (unsigned char)~(0x01 << pin);
}

/**
 * Digital read.
 * 
 * Read the value of the given pin, which will be 1 or 0 depending on the
 * logic level voltage applied to the pin.
 * 
 * *NOTE
 * ------
 * readPin returns 0 then a pin is Low, and 1 then a pin is High.
 * 
 */
int readPin(volatile unsigned char *port, unsigned char pin)
{
    return (*port >> pin) & 0x01;
}