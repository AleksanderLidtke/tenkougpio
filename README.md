# README #

This is a library that provides enables digital input and output operations on arbitrary general purpose 
input-output (GPIO) pins of a PIC microcontroller without hard-coding the pin numbers. It's intended for
use with a PIC16F877A on-board the Tenkou small satellite of the Kyushu Institute of Technology.

### Version history ###
Versions correspond to git tags.

* v1.0.0 - finished prototyping the library. Defined the interfaces and tested that it works with TenkouSPISlave v.1.0.0 and TenkouSPIMaster v.1.0.0

### How do I get set up? ###

* There are no prerequisites, but please clone it to a folder called `tenkougpio` not to break links with SPI MPLAB projects.
* The library is tested with the free version of the XC8 compiler.

### Whom do I talk to? ###

Any doubts, questions or issues talk to Alek, Urakami or Rafa.